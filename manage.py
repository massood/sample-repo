#!/usr/bin/env python3
import click


@click.group()
def cli():
    """CLI for sample app"""
    pass


@cli.command()
def run():
    click.echo("Starting service...")
    click.echo("Service started.")


@cli.command()
def test():
    click.echo("Running tests...")
    click.echo("OK")


if __name__ == '__main__':
    cli()
