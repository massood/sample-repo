FROM    python:latest

ENV     LC_ALL=C.UTF-8 LANG=C.UTF-8

ADD     requirements.txt /app/
RUN     pip install -r /app/requirements.txt

ADD     . /app/
WORKDIR /app/

CMD     ["./manage.py run"]
